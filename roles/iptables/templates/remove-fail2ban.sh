#!/bin/sh
#
# {{ ansible_managed }}
#

set -xe

IP="$1"

# iptables:
iptables -D f2b-default -s "$IP" -j REJECT

# hosts.deny:
sed -i.old "/ALL:\ $IP/d" /etc/hosts.deny
