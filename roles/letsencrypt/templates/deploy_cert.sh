#!/bin/bash
#
# {{ ansible_managed }}
#
# This script is to be called from the hook script of letsencrypt.sh, and
# takes care of actually deploying the certificates and maybe backing them up.

set -e
set -u
set -o pipefail

DOMAIN="${1}"
# The path of the file containing the private key.
KEYFILE="$(readlink -e "${2}")" || (echo "$2 does not seem to exist!" >&2 && exit 1)
# The path of the file containing the signed certificate.
CERTFILE="$(readlink -e "${3}")" || (echo "$3 does not seem to exist!" >&2 && exit 1)
# The path of the file containing the full certificate chain.
FULLCHAINFILE="$(readlink -e "${4}")" || (echo "$4 does not seem to exist!" >&2 && exit 1)
# The path of the file containing the intermediate certificate(s).
CHAINFILE="$(readlink -e "${5}")" || (echo "$5 does not seem to exist!" >&2 && exit 1)

_contains () {
    local n=$#
    local value=${!n}
    for ((i=1; i < $#; i++)) {
        if [ "${!i}" == "${value}" ]; then
            return 0
        fi
    }
    return 1
}

_log () {
    echo " + (deploy_cert) $*"
}

reload_apache () {
    _log "Reloading apache..."
    sudo apache2ctl graceful
}

reload_postfix () {
    _log "Reloading postfix..."
    sudo service postfix reload
}

{% if inventory_hostname == "kahlan.mapreri.org" %}
update_znc () {
    _log "Upading cert for znc..."
    local tmpkey
    tmpkey=$(mktemp -p /srv/znc/ssl tmp.XXXXXXXXX)
    chmod 660 "${tmpkey}"
    cat "${KEYFILE}" "${CERTFILE}" "${CHAINFILE}" > "${tmpkey}"
    mv -f "${tmpkey}" /srv/znc/ssl/znc.pem
}
{% endif %}

reload_apache
if [ "${DOMAIN}" = "$(hostname -f)" ]; then
    reload_postfix
fi
{% if inventory_hostname == "kahlan.mapreri.org" %}
if [ "${DOMAIN}" = "irc.mapreri.org" ]; then
    update_znc
fi
{%- endif %}
