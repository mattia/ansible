#!/bin/bash
#
# {{ ansible_managed }}

set -e
set -u
set -o pipefail

OP="$1"
shift

# shellcheck disable=SC1091
. "/etc/dehydrated/config"
# shellcheck disable=SC1091
. "/etc/dehydrated/conf.d/config.sh"
# don't deploy certs for these domains
DOMAINS_NO_DEPLOY=(""
)
# domains where a manual procedure to publish the challeng is needed
DOMAINS_MANUAL_CHALLENGE=(""
)


_contains () {
    local n=$#
    local value=${!n}
    for ((i=1; i < $#; i++)) {
        if [ "${!i}" == "${value}" ]; then
            return 0
        fi
    }
    return 1
}

_log () {
    echo " + (${OP}) $*"
}

function deploy_challenge {
    local TOKEN_FILENAME="${1}" TOKEN_VALUE="${2}" tmp

    # This hook is called once for every domain that needs to be
    # validated, including any alternative names you may have listed.
    #
    # Parameters:
    # - TOKEN_FILENAME
    #   The name of the file containing the token to be served for HTTP
    #   validation. Should be served by your web server as
    #   /.well-known/acme-challenge/${TOKEN_FILENAME}.
    # - TOKEN_VALUE
    #   The token value that needs to be served for validation. For DNS
    #   validation, this is what you want to put in the _acme-challenge
    #   TXT record. For HTTP validation it is the value that is expected
    #   be found in the $TOKEN_FILENAME file.

    if (_contains "${DOMAINS_MANUAL_CHALLENGE[@]}" "${DOMAIN}"); then
        _log "Manual deploy challenge needed for ${DOMAIN}:"
        _log "Put ${TOKEN_VALUE}"
        _log "In ${TOKEN_FILENAME}"
        _log "You have 30 seconds!"
        printf "\t"
        local s
        for s in $(seq 0 30); do
            printf "%s " "$s"
            sleep 1
        done
        printf "\n"
        _log "Continuing now..."
    fi
    if [[ "$TOKEN_VALUE" =~ [.] ]]; then
        :
    else
        # DNS challenges don't have a . in them.
        if [ -t 0 ]; then
            _log "Add the following to the zone definition of ${DOMAIN}:"
            _log "_acme-challenge.${DOMAIN}. IN TXT \"${TOKEN_VALUE}\""
            _log ""
            _log "Press enter to continue..."
            read -r tmp
            _log ""
        else
            _log "This requires a working terminal!"
            exit 1
        fi
    fi

}

function clean_challenge {
    local TOKEN_FILENAME="${1}" TOKEN_VALUE="${2}" tmp

    # This hook is called after attempting to validate each domain,
    # whether or not validation was successful. Here you can delete
    # files or DNS records that are no longer needed.
    #
    # The parameters are the same as for deploy_challenge.
    if (_contains "${DOMAINS_MANUAL_CHALLENGE[@]}" "${DOMAIN}"); then
        _log "Manual deploy challenge done for ${DOMAIN}:"
        _log "Remember to clean up the challenge tokens for ${DOMAIN} from ${TOKEN_FILENAME}."
    fi
    if [[ "$TOKEN_VALUE" =~ [.] ]]; then
        :
    else
        # DNS challenges don't have a . in them.
        if [ -t 0 ]; then
            _log "Now you can remove the following from the zone definition of ${DOMAIN}:"
            _log "_acme-challenge.${DOMAIN}. IN TXT \"${TOKEN_VALUE}\""
            _log ""
            _log "Press enter to continue..."
            # shellcheck disable=SC2034  # unused variable tmp
            read -r tmp
            _log ""
        else
            _log "This requires a working terminal!"
            exit 1
        fi
    fi

}

deploy_cert () {
    if (_contains "${DOMAINS_NO_DEPLOY[@]}" "${DOMAIN}"); then
        _log "This certificates is not to be deployed, skipping."
        exit 0
    fi
    "/etc/dehydrated/deploy_cert.sh" "${DOMAIN}" "$@"
}


case "$OP" in
    deploy_challenge|clean_challenge|deploy_cert)
        DOMAIN="${1}"
        shift
        "$OP" "$@"
        ;;
    unchanged_cert|generate_csr|invalid_challenge|request_failure|startup_hook|exit_hook|sync_cert)
        ;;
    this_hookscript_is_broken__dehydrated_is_working_fine__please_ignore_unknown_hooks_in_your_script)
        ;;
    *)
        echo "WARN: unknown operation: $OP" >&2
        ;;
esac
