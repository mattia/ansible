#!/bin/bash

#
# {{ ansible_managed }}
#


set -eu

BACKUPHOST=backuphost.mapreri.org

HOSTNAME="$(hostname -f)"
if ! echo "$HOSTNAME" | grep -q . ; then
    echo \`hostname -f\` did not return a valid hostname >&2
    exit 1
fi

export BORG_REPO="${BACKUPHOST}:backups/${HOSTNAME}"
if [ -n "${ARCHIVE:-}" ]; then
    echo "Warning: using ARCHIVE name coming from the environment: '$ARCHIVE'" >&2
else
    ARCHIVE='General-{now:%Y-%m-%d}'
fi

BORG=borg

pwd_file='{{ pwfile }}'
if [ ! -f "$pwd_file" ]; then
    echo "$pwd_file does not exist" >&2
    exit 1
elif [ "$(stat --format='%u:%g_%U:%G_%a' "$pwd_file")" != "0:0_root:root_400" ]; then
    echo "$pwd_file doesn't have the correct permissions (root:root 400):" >&2
    stat "$pwd_file" >&2
    exit 1
else
    # finally the file is good enough for us
    BORG_PASSPHRASE="$(< "$pwd_file")"
    export BORG_PASSPHRASE
    if [ -z "$BORG_PASSPHRASE" ]; then
        echo "$pwd_file is empty" >&2
        exit 1
    fi
fi

if ! wget -q --spider https://google.com ; then
    echo "Network not available, trying to wait two minutes and see if it's back" >&2
    sleep 2m
fi

# the borg process will return more specific exit codes (rc).  Default is
# "legacy" and returns rc 2 for all errors, 1 for all warnings, 0 for success.
export BORG_EXIT_CODES=modern

b_init(){
    "$BORG" init -v --show-rc --encryption repokey
}
b_list() {
    if [ -z "$*" ]; then
        # listing archives
        set -x
        "$BORG" list
    else
        # most likely listing files
        set -x
        "$BORG" list \
            --format="{mode} {size:8d} {isomtime} {path}{extra}{NEWLINE}" \
            "$@"
    fi
}
b_create() {
    # `borg help patterns` for help with exclusion patterns
    "$BORG" create \
        -v \
        --stats \
        --show-rc \
        --progress \
        --compression zlib \
        --exclude-caches \
        --exclude-if-present .nobackup \
        --exclude '*.pyc' \
        --exclude '/var/cache/' \
        --exclude '/var/lib/apt/lists' \
        --exclude '/var/lib/munin/*.tmp' \
        --exclude '/var/lib/docker' \
        --exclude '/home/*/Downloads' \
        --exclude '/home/*/tmp' \
        --exclude '/home/*/.cache' \
        --exclude '/home/*/.local/share/Trash' \
        "::${ARCHIVE}" \
        /etc \
        /home \
        /root \
        /srv \
        /usr \
        /var \
        "$@"
}
b_prune() {
    # Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
    # archives of THIS machine.
    "$BORG" prune \
        -v \
        --list \
        --stats \
        --show-rc \
        --keep-within 30d \
        --keep-daily 7 \
        --keep-weekly 4 \
        --keep-monthly 12 \
        --keep-yearly 20 \
        "$@"
}
b_compact() {
    echo "Compacting the repository..."
    "$BORG" compact \
        -v \
        --show-rc
}
b_break-lock() {
    exec "$BORG" break-lock -v --show-rc
}
b_delete() {
    exec "$BORG" delete -v --stat --show-rc "$@"
}

text_help() {
    echo Available commands:
    echo "    help              show this help text"
    echo "    init"
    echo "    list"
    echo "    create"
    echo "    prune"
    echo "    compact"
    echo "    break-lock"
    echo "    delete"
    echo "    --                ignore all previous arguments and pass everything to borg"
    echo "    - all other words not listed here are passed to borg as-is."
}

if [ $# -gt 0 ]; then
    case "$1" in
        -h|help)
            text_help
            exit 0
            ;;
        init)
            b_init
            ;;
        list)
            shift
            b_list "$@"
            ;;
        create)
            shift
            b_create "$@"
            ;;
        prune)
            shift
            b_prune "$@"
            ;;
        break-lock)
            shift
            b_break-lock "$@"
            ;;
        delete)
            shift
            b_delete "$@"
            ;;
        --)
            shift
            set -x
            exec "$BORG" "$@"
            ;;
        *)
            exec "$BORG" "$@"
            ;;
    esac
    exit $?
fi

# default action with none specified
retcode=0
b_create || retcode=$?
case "$retcode" in
    0)
        : ;;
    100)
        # exit code 100 is "file changed while we backed it up"
        # don't fail the whole backup over it.
        : ;;
    *)
        exit "$retcode" ;;
esac
b_prune
b_compact
