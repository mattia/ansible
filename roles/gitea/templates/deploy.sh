#!/bin/bash
#
# {{ ansible_managed }}
#

set -uxe

version=${1:?missing version}; shift
base_dir="{{ gitea.base_dir }}"
deploy_path="$base_dir/bin"

signing_key="$base_dir/conf/signing-key.pgp"
if [ ! -f "$signing_key" ]; then
    echo "Missing $signing_key" >&2
    exit 1
fi

tmpdir=$(mktemp -d)
trap 'rm -r $tmpdir' EXIT

filename="$tmpdir/gitea_${version}.bin"

wget -c "https://dl.gitea.io/gitea/$version/gitea-${version}-linux-amd64" -O "$filename"
chmod 555 "$filename"
wget -c "https://dl.gitea.io/gitea/$version/gitea-${version}-linux-amd64.asc" -O "$filename.asc"

gpgv --homedir "/dev/null" --keyring "$signing_key" "$filename.asc" "$filename"


cp -v "$filename" "$deploy_path/"
ln -sfv "$(basename "$filename")" "$deploy_path/gitea"


# restart gitea on the new version
sudo -u git XDG_RUNTIME_DIR="/run/user/$(id -u git)" systemctl --user restart gitea.service
