#
# {{ ansible_managed }}
#

Host salsa.debian.org
    # not putting 'User git' here is an explicit choice so I specify it per-repo
    IdentityFile ~/.ssh/id_ed25519_salsa
    IdentitiesOnly yes

Host qa
    HostName qa.debian.org
    GlobalKnownHostsFile /etc/ssh/debian.org_known_hosts
    UserKnownHostsFile /dev/null
    StrictHostKeyChecking yes
    UpdateHostKeys no
    VerifyHostKeyDNS ask

Host debomatic64
    HostName debomatic-amd64.debian.net
    User debomatic

Host kahlan
    HostName kahlan.mapreri.org

Host savidlin
    HostName savidlin.mapreri.org

Host code.ubuntu-it.org
    User git
    Port 2222

Host owncloud
    HostName owncloud.mapreri.org
    Port 5555

Host spock spock.ubuntu-it.org
    HostName code.ubuntu-it.org
    User root
    Port 2222

Host bromuro bromuro.ubuntu-it.org
    HostName roadhouse.ubuntu-it.org
    User root
    Port 2223

## jenkins.debian.net hosts
# SSH keys are available from the j.d.n.git repository, run
# nodes/gen_known_host_file to create/update it.
Host jenkins
    StrictHostKeyChecking yes
    HostName jenkins.debian.net

Host cs-jumpserv jumpserv.colo.codethink.co.uk
    HostName jumpserv.colo.codethink.co.uk
    User reproducible
    Port 22101

{% for host in groups['jenkins_nodes'] %}
{% set split_host = host.split('-')[0] %}
{% if hostvars[host]['ssh_shortname'] is defined %}
{% set ssh_shortname = hostvars[host]['ssh_shortname'] %}
{% elif split_host.startswith('ionos') %}
{% set builder = split_host|list %}
{% set ssh_shortname = 'pb' + ''.join(builder[5:]) %}
{% elif split_host.startswith('osuosl') %}
{% set builder = split_host|list %}
{% set ssh_shortname = 'o' + ''.join(builder[6:]) %}
{% elif split_host.startswith('codethink') %}
{% set builder = split_host|list %}
{% set ssh_shortname = 'cs' + ''.join(builder[9:]) %}
{% else %}
{% set ssh_shortname = split_host %}
{% endif %}
Host {{ ssh_shortname }} {{ host }}
    HostName {{ host }}
    StrictHostKeyChecking yes
{% if hostvars[host]['ansible_ssh_port']|default(22) != 22 %}
    Port {{ hostvars[host]['ansible_ssh_port'] }}
{% endif %}
{% endfor %}

Host *
  ServerAliveInterval 240
  HashKnownHosts no
