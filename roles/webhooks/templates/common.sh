#!/bin/bash

#
# {{ ansible_managed }}
#

set -euo pipefail

logdir="{{ webhooks.logdir }}"

#{% raw %}

now() {
    date -u -R
}

starttime=$(date -u '+%Y%m%dT%H%M%SZ')
startepoch=$(date -u '+%s')

: "${HOOK_NAME:?missing HOOK_NAME}"

if [ ! -O "$logdir" ]; then
    echo "User not matching, re-execing"
    exec sudo -u "$(stat -c '%U' "$logdir")" "$(readlink -f "$MYSELF")" "$@"
fi
[ -d "$logdir/$HOOK_NAME" ] || mkdir -p "$logdir/$HOOK_NAME"

# This variable should be set by the webhook server.
# It normally matches X-Gitea-Delivery from the webhook HTTP request header.

if [ -n "${DELIVERY_ID:-}" ]; then
    logfile="$logdir/$HOOK_NAME/${starttime}_${DELIVERY_ID}_$$.log"
else
    logfile="$logdir/$HOOK_NAME/${starttime}_$$.log"
fi

# don't spam stdout if not running on terminal
if [ -t 0 ]; then
    exec &> >(tee "$logfile")
else
    exec &> "$logfile"
fi

echo "Running task $HOOK_NAME with $MYSELF"
echo "Started at $(date -u -d "@$startepoch" -R)"

: parsing the environment
HOOK_TYPE=${HOOK_TYPE:?missing HOOK_TYPE}
REPOSITORY_NS=${HOOK_REPOSITORY_NS:?missing REPOSITORY_NS}
REPOSITORY_NAME=${HOOK_REPOSITORY_NAME:?missing REPOSITORY_NAME}
CLONE_URL=${HOOK_CLONE_URL:?missing CLONE_URL}
BRANCH=${HOOK_BRANCH:?missing BRANCH}
case "$HOOK_TYPE" in
    push)
        COMMIT=${HOOK_COMMIT:?missing COMMIT}
        PUSHER_EMAIL=${HOOK_PUSHER_EMAIL:?missing PUSHER_EMAIL}
        ;;
    delete)
        PUSHER_EMAIL=${HOOK_SENDER_EMAIL:?missing SENDER_EMAIL}
        ;;
    *)
        echo "Unhandled HOOK_TYPE: $HOOK_TYPE" >&2
        exit 1
esac


# sanity check for BRANCH
case "$HOOK_TYPE" in
    delete)
        BRANCHNAME=$BRANCH
        # shellcheck disable=SC2034
        BRANCHNAME_SAFE=${BRANCHNAME//\//_}
        ;;
    push)
        if [[ "$BRANCH" =~ ^refs/heads/.+ ]]; then
            BRANCHNAME=${BRANCH#refs/heads/}
            # shellcheck disable=SC2034
            BRANCHNAME_SAFE=${BRANCHNAME//\//_}
        else
            echo "Running on non-branches is not supported" >&2
            exit 1
        fi
        ;;
esac

# TMPDIR is obeyed by mktemp, so future call don't need to use -p
TMPDIR=$(mktemp -d -t "webook.${HOOK_NAME}.XXXXXXXX")
cleanup_tempdir() { rm -rf "$TMPDIR"; }
trap 'cleanup_tempdir' EXIT
export TMPDIR


clone_repo() {
    # clone into TMPDIR/repo, sets COMMITDATE
    echo "+++ Setting up the repository"

    pushd "$TMPDIR"
    git clone --single-branch --branch "$BRANCHNAME" "$CLONE_URL" "${@}" repo

    pushd repo
    git checkout "$COMMIT"
    COMMITDATE=$(git log -1 --pretty=%cd --date=iso-strict)
    popd ; popd
}

email_log() {
    if [ "${NOMAIL:-no}" = yes ]; then
        return 0
    fi
    local mailbody endepoch
    mailbody=$(mktemp -t email.XXXXX)
    endepoch=$(date -u '+%s')
    cat > "$mailbody" <<- EOF
	Dear committer,

	The task $HOOK_NAME ${DELIVERY_ID:+with ID "$DELIVERY_ID" }terminated.

EOF

    if [ "$HOOK_TYPE" = push ]; then
        cat >> "$mailbody" <<- EOF
	The task was triggered by commit $COMMIT
	on branch $BRANCHNAME ${COMMITDATE+(made on $COMMITDATE) }in $REPOSITORY_NS/$REPOSITORY_NAME.
EOF
    elif [ "$HOOK_TYPE" = delete ]; then
        cat >> "$mailbody" <<- EOF
	The task was triggered by the deletion of branch $BRANCHNAME
	from $REPOSITORY_NS/$REPOSITORY_NAME.
EOF
    fi

#{% endraw %}

    cat >> "$mailbody" <<- EOF

	The task started on $(date -u -d "@$startepoch" -R)
	The task ended on $(date -u -d "@$endepoch" -R).
	The task took $(date -u -d "0 $endepoch seconds - $startepoch seconds" +"%H:%M:%S").

	${EXTRA_EMAIL_FILE:+$(< "$EXTRA_EMAIL_FILE")}


	Following is the full log of the task.

	$(< "$logfile")

	-- 
	Your Gitea administrator
	EOF
    < "$mailbody" mail -s "Webhook $HOOK_NAME completed" \
        -r "{{ webhooks.mail_from }}" \
        "$PUSHER_EMAIL"
}
finishup(){
    email_log
    cleanup_tempdir
}
trap 'finishup' EXIT
