#!/bin/bash

set -euo pipefail

f=${1:?key file to rename}

if [ ! -f "$f" ]; then
    echo "$f is not a file." >&2
    exit 1
fi

mapfile -t hashes < <(ssh-keygen -f "$f" -l -E md5|awk '{print $2}'|cut -d: -f2-| tr -d :)

if [ ${#hashes[@]} -ne 1 ]; then
    echo "Only one key per file is supported.  Found: ${#hashes[@]}." >&2
    exit 1
fi

mv -v "$f" "${f}-${hashes[0]}"
